import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categories: string[] = [];
  item = {};

  constructor(private httpClient: HttpClient, private log: LoggerService) { }

  getCategories(): Observable<string[]> {
    this.log.log('CategoryService', 'returning a collection of categories');
    return this.httpClient.get<string[]>('https://swapi.co/api/');
  }

  getItem(category: string, id: number): Observable<string[]> {
    this.log.log('CategoryService', 'getting requested item');
    return this.httpClient.get<string[]>(`https://swapi.co/api/${category}/${id}`);
  }
}
