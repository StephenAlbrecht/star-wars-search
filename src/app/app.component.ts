import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CategoryService } from './common/category-service/category-service.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'star-wars-search';
  item = {};
  catKeys = Object.keys;
  categories : string[] = [];
  selectedCat : string = 'people';
  id: number;

  constructor(private cs: CategoryService, private http : HttpClient) {
    cs.getCategories().subscribe(response=> {
      this.categories = response;
    })
  }

  search(f: FormGroup) : void {
    this.cs.getItem(this.selectedCat, this.id).subscribe(response => {
      this.item = response;
    });
  }
}
