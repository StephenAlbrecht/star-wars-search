# Star Wars Search

This simple Angular project displays a search form to expose data using the the [Star Wars API](https://swapi.co/api/).

The dropdown list is populated with the major API categories via a get request (so may take some time).

Only the "Name" or "Title" property will be displayed upon search. Unfortunately, no error handling was implemented to catch 404 requests.

## Problems encountered

After initially creating a "Search" component for the search form, I found difficulty in figuring out where to pass the results of the search. I had wanted the results to be displayed by a sibling "Display" component, but couldn't figure it out in time. To get things moving, I moved the contents of the "Search" component to the parent "app.component" which could both perform the search and display the results below. 